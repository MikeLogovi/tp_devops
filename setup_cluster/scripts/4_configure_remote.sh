#!/bin/bash

# Nous obtenons le password a partir du Master Node et un peu de text processing
PASSWORD=$(vagrant ssh kubemaster1 -c " sudo grep password /etc/rancher/k3s/k3s.yaml" | awk -F ':' '{print $2}' | sed 's/ //g')

cat << EOF > /tmp/kubectlvagrantconfig.yml
--- 
apiVersion: v1
clusters: 
  - 
    cluster: 
      insecure-skip-tls-verify: true
      name: default
      server: "https://25.0.0.30:6443"
contexts: 
  - 
    context: 
      cluster: default
      user: default
    name: default
current-context: default
kind: Config
preference: {}
users: 
  - 
    name: default
    user: 
      password: "${PASSWORD}"
      username: admin

EOF

export KUBECONFIG=/tmp/kubectlvagrantconfig.yml